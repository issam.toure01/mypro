
# Rapport de TP Devops

## Création du Dockerfile et Compilation

```dockerfile
FROM alpine
ENTRYPOINT ["date"]
```

Exécution de la commande pour créer l'image Docker :

```bash
docker build -t afficher-date .
```

Cette commande crée une image Docker nommée `afficher-date` basée sur l'instruction fournie dans le Dockerfile.

Pour vérifier l'existence de l'image :

```bash
docker images
```

On voit `afficher-date` dans la liste des images disponibles.

Pour exécuter l'image et supprimer le conteneur après son exécution :

```bash
docker run --rm afficher-date
```

L'option `--rm` garantit que le conteneur est supprimé après son exécution, ce qui est utile pour ne pas encombrer votre système avec des conteneurs inutilisés.

## b) Modification avec CMD

```dockerfile
FROM alpine
ENTRYPOINT ["date"]
CMD ["+%d-%m-%Y"]
```

`ENTRYPOINT` définit la commande de base (`date`) et `CMD` fournit les paramètres par défaut (`+%d-%m-%Y`) à cette commande.
