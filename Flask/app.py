from flask import Flask, jsonify
app = Flask(__name__)

@app.route('/<string:name>', methods=['GET'])
def greet(name):
    return jsonify(message=f"Hello, {name}!")

if __name__ == '__main__':
    app.run(host='0.0.0.0')
